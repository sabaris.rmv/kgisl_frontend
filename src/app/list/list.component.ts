import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../service/http-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  appointments: Array<any> = [];


  constructor(
    private http: HttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.http.get("appointments")
      .subscribe((data: any) => {
        console.log(data);
        if (data.success) {
          this.appointments = data.list;
        } else {
          this.appointments = []
        }
      }, error => {
        console.log(error);
      })
  }

  create() {
    this.router.navigate(['create']);
  }

}
