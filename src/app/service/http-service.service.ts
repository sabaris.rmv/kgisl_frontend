import { Injectable } from '@angular/core';
import { HttpClient,  HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class HttpService {

    // Variable Declaration
    hostUrl: string;

    constructor(
        private http: HttpClient,
    ) {
        this.hostUrl = "http://localhost:3150/api/v1/";
    }

    get(url) {
        return this.http.get(this.hostUrl + url);
    }

    post(url, data) {
        return this.http.post(this.hostUrl + url, data);
    }
 
   errorMgmt(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
}