import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { from } from 'rxjs';
import { DatePipe } from '@angular/common'
import { HttpService } from '../service/http-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  name: any = null;
  email: any = null;
  phone: any = null;
  date: any = null;
  time: any = null;

  datePickerConfig = {
    firstDayOfWeek: 'su',
    monthFormat: 'MMM, YYYY',
    disableKeypress: false,
    allowMultiSelect: false,
    closeOnSelect: undefined,
    closeOnSelectDelay: 100,
    onOpenDelay: 0,
    weekDayFormat: 'ddd',
    // appendTo: document.body,
    drops: 'down',
    opens: 'right',
    showNearMonthDays: true,
    showWeekNumbers: false,
    enableMonthSelector: true,
    format: "DD/MM/YYYY",
    yearFormat: 'YYYY',
    showGoToCurrent: true,
    dayBtnFormat: 'DD',
    monthBtnFormat: 'MMM',
    hours12Format: 'hh',
    hours24Format: 'HH',
    meridiemFormat: 'A',
    minutesFormat: 'mm',
    minutesInterval: 1,
    secondsFormat: 'ss',
    secondsInterval: 1,
    showSeconds: false,
    showTwentyFourHours: true,
    timeSeparator: ':',
    multipleYearsNavigateBy: 10,
    showMultipleYearsNavigation: false,

    // locale: 'zh-cn',
    // min:'2017-08-29 15:50',
    // minTime:'2017-08-29 15:50'
  };
  selectedDate: Date;

  constructor(
    public datepipe: DatePipe,
    private http: HttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  submit(form: NgForm) {
    console.log(form);
    if (form.valid) {
      let body = {
        email: form.value.email,
        phone: form.value.phone,
        name: form.value.name,
        appointment_time: form.value.time,
        appointment_date: this.datepipe.transform(form.value.selectedDate, 'dd/MM/yyyy'),
      }
      console.log("body",body)
      this.http.post("appointments", body)
        .subscribe((data: any) => {
          console.log(data);
          if (data.success) {
            this.router.navigate(['list']);
          } else {
            alert(data.message)
            this.router.navigate(['list']);
          }
        }, (error) => {
          alert(error?.error?.message)
          // this.router.navigate(['list']);
          console.log(error);
        })
    }
  }

  back(){
    this.router.navigate(['list']);
  }

}
